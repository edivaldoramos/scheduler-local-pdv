package br.com.ithappens.pdv.scheduler.task;

import java.io.IOException;

public interface IImportacaoCargaClienteTask {
    /**
     * Implementação deverá fornecer forma de importr uma carga para o PDV.
     *
     * @author Allyx Cristiano
     */
    void importarCarga() throws IOException, InterruptedException;
}
