package br.com.ithappens.pdv.scheduler.mapper;

import br.com.ithappens.pdv.scheduler.model.Cliente;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ClienteMapper {
    boolean clienteExiste(@Param("id") Long id);

    void atualizar(@Param("cliente") Cliente cliente);

    void inserir(@Param("cliente") Cliente cliente);
}