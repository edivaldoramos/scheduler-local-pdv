package br.com.ithappens.pdv.scheduler.mapper.typeHandler;

import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TipoCargaTypeHandler extends BaseTypeHandler<TipoCarga> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, TipoCarga parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getCodigo());
    }

    @Override
    public TipoCarga getNullableResult(ResultSet rs, String columnName) throws SQLException {
        TipoCarga tipoCarga = TipoCarga.fromCodigo(rs.getInt(columnName));
        if (rs.wasNull())
            return null;
        return tipoCarga;
    }

    @Override
    public TipoCarga getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        TipoCarga tipoCarga = TipoCarga.fromCodigo(rs.getInt(columnIndex));
        if (rs.wasNull())
            return null;
        return tipoCarga;
    }

    @Override
    public TipoCarga getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        TipoCarga tipoCarga = TipoCarga.fromCodigo(cs.getInt(columnIndex));
        if (cs.wasNull())
            return null;
        return tipoCarga;
    }
}
