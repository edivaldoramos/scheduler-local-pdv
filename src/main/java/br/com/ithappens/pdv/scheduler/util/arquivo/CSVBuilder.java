package br.com.ithappens.pdv.scheduler.util.arquivo;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.extern.slf4j.Slf4j;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Slf4j
public class CSVBuilder<T> implements ICSVBuilder<T> {

    private final Class<T> tipoClasse;
    private String[] headers;
    private String[] campos;
    private String caminho;
    private FileWriter fileWriter;
    private CSVWriter csvWriter;
    private ColumnPositionMappingStrategy<T> mappingStrategy;
    private StatefulBeanToCsv<T> cargaClienteDTOStatefulBeanToCsv;

    /**
     * Construtor necessário para que seja armazenado o tipo da classe pois a classe utilitárias que construirá o CSV
     * precisa desta tipagem e não consigo obtê-la diretamente pelo Generics <T>.
     *
     * @param tipoClasse
     * @author Allyx Cristiano
     */
    public CSVBuilder(Class<T> tipoClasse) {
        this.tipoClasse = tipoClasse;
    }

    @Override
    public ICSVBuilder adicionarLinhaHeader(String[] headers) {
        this.headers = headers;
        return this;
    }

    @Override
    public ICSVBuilder adicionarCamposLeitura(String[] campos) {
        this.campos = campos;
        return this;
    }

    @Override
    public ICSVBuilder adicionarCaminhoSalvarArquivo(String caminho) {
        this.caminho = caminho;
        return this;
    }

    @Override
    public ICSVBuilder construir(List lista) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        FileWriter fileWriter = new FileWriter(this.caminho);

        CSVWriter csvWriter = new CSVWriter(fileWriter, '|', '"', CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
        csvWriter.writeNext(headers);

        ColumnPositionMappingStrategy<T> mappingStrategy = new ColumnPositionMappingStrategy<T>();

        mappingStrategy.setType(this.tipoClasse);
        mappingStrategy.setColumnMapping(campos);

        StatefulBeanToCsv<T> cargaClienteDTOStatefulBeanToCsv = new StatefulBeanToCsvBuilder<T>(fileWriter)
                .withMappingStrategy(mappingStrategy)
                .withQuotechar('"')
                .withSeparator('|')
                .build();

        cargaClienteDTOStatefulBeanToCsv.write(lista);

        if (fileWriter != null) fileWriter.close();

        return this;
    }

    @Override
    public ICSVBuilder construir(T valor) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        if (fileWriter == null) {
            this.fileWriter = new FileWriter(this.caminho);

            this.csvWriter = new CSVWriter(fileWriter, '|', '"', CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
            this.csvWriter.writeNext(headers);

            this.mappingStrategy = new ColumnPositionMappingStrategy<T>();
            this.mappingStrategy.setType(this.tipoClasse);
            this.mappingStrategy.setColumnMapping(campos);

            cargaClienteDTOStatefulBeanToCsv = new StatefulBeanToCsvBuilder<T>(fileWriter)
                    .withMappingStrategy(mappingStrategy)
                    .withQuotechar('"')
                    .withSeparator('|')
                    .build();
        }

        cargaClienteDTOStatefulBeanToCsv.write(valor);

        return this;
    }

    @Override
    public void closeFileWriter() throws IOException {
        if (fileWriter != null) fileWriter.close();
    }
}