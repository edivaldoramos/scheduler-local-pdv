package br.com.ithappens.pdv.scheduler.service;

import br.com.ithappens.pdv.scheduler.mapper.ClienteMapper;
import br.com.ithappens.pdv.scheduler.model.Cliente;
import br.com.ithappens.pdv.scheduler.model.dto.wsmateus.FileData;
import br.com.ithappens.pdv.scheduler.model.enums.TipoPessoa;
import br.com.ithappens.pdv.scheduler.service.validation.IValicaoArquivoCargaClienteService;
import br.com.ithappens.pdv.scheduler.util.IWSMateus;
import br.com.ithappens.pdv.scheduler.util.arquivo.ArquivoUtil;
import br.com.ithappens.pdv.scheduler.util.arquivo.IArquivoUtil;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@Slf4j
public class ImportadorArquivoCargaClienteService extends ImportadorArquivoCargaService {
    @Autowired
    IWSMateus wsMateus;

    @Autowired
    ClienteMapper clienteMapper;

    @Autowired
    IValicaoArquivoCargaClienteService valicaoArquivoCargaClienteService;

    @Override
    URL getUrlDownloadCompleto() throws MalformedURLException {
        return new URL("http://127.0.0.1:8086/clientes/hash/busca/");
    }

    @Override
    URL getUrlDownloadPorData(LocalDateTime data) throws MalformedURLException {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        System.out.println(data.format(dateTimeFormatter));
        return new URL("http://127.0.0.1:8086/clientes/hash/busca/" + data.format(dateTimeFormatter));
    }

    @Override
    String getHashCarga() throws IOException {
        return this.download(this.getUrlDownloadCompleto());
    }

    @Override
    String getHashCargaPorData(LocalDateTime data) throws IOException {
        return this.download(this.getUrlDownloadPorData(data));
    }

    @Override
    FileData efetuaDownload(String hash) throws IOException {
        FileData arquivo = wsMateus.download(hash);
        File file = new File(this.getCaminhoPastaUsuario() + "/" + arquivo.getNomeDoArquivo());
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
        bufferedOutputStream.write(arquivo.getConteudo());
        bufferedOutputStream.close();
        return arquivo;
    }

    @Override
    void descompacta(FileData arquivo) throws IOException {
        File file = new File(this.getCaminhoPastaUsuario() + "/" + arquivo.getNomeDoArquivo());
        IArquivoUtil arquivoUtil = new ArquivoUtil();
        arquivoUtil.descompactar(file, this.getCaminhoPastaUsuario());
    }

    @Override
    void atualizaBancoDadosLocalComDadosArquivoCSV() throws IOException {
        CSVReader csvReader = this.getCsvReader();
        String[] linha;

        while ((linha = csvReader.readNext()) != null) {

            linha = this.getLinhaComCamposComConteudoAjustado(linha);

            Cliente cliente = new Cliente();
            cliente.setId(Long.parseLong(linha[0]));
            cliente.setRazaoSocial(linha[1]);
            cliente.setNomeFantasia(linha[2]);

            TipoPessoa tipoPessoa = linha[3] == "F" ? TipoPessoa.CPF : TipoPessoa.CNPJ;
            cliente.setTipoPessoa(tipoPessoa);

            cliente.setCpfCnpj(Long.parseLong(linha[4]));

            if (clienteMapper.clienteExiste(cliente.getId())) {
                clienteMapper.atualizar(cliente);
            } else {
                clienteMapper.inserir(cliente);
            }
        }
    }

    @Override
    String getNomeArquivoCSV() {
        return "clientes.csv";
    }

    @Override
    String[] getLinhaComCamposComConteudoAjustado(String[] linha) {
        valicaoArquivoCargaClienteService.validarLinhaTemQuantidadeEsperadaCampos(linha);

        linha = super.getLinhaComCamposComConteudoAjustado(linha);

        String razaoSocial = linha[1];
        if (razaoSocial.length() > 200) {
            linha[1] = razaoSocial.substring(0, 199);
        }

        String nomeFantasia = linha[2];
        if (nomeFantasia.length() > 200) {
            linha[2] = nomeFantasia.substring(0, 199);
        }

        String cpfCnpj = linha[4];
        if (cpfCnpj.isEmpty()) {
            linha[4] = "0";
        }

        return linha;
    }
}