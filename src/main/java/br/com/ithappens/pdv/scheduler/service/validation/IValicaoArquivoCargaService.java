package br.com.ithappens.pdv.scheduler.service.validation;

import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.service.exception.CargaComImportacaoInterrompidaException;
import br.com.ithappens.pdv.scheduler.service.exception.CargaNuncaImportadaException;

import java.io.IOException;
import java.net.HttpURLConnection;

public interface IValicaoArquivoCargaService {
    void validarCargaReferenciaParaImportacao(Carga carga) throws CargaNuncaImportadaException, CargaComImportacaoInterrompidaException;

    void validarHash(String hash);

    void validarProcessamentoExterno(HttpURLConnection httpURLConnection) throws IOException;
}
