package br.com.ithappens.pdv.scheduler.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum TipoPessoa {
    CPF(0), CNPJ(1);

    private static Map<Integer, TipoPessoa> lookup;

    static {
        lookup = new HashMap<Integer, TipoPessoa>();
        EnumSet<TipoPessoa> enumSet = EnumSet.allOf(TipoPessoa.class);
        for (TipoPessoa tipoPessoa : enumSet) {
            lookup.put(tipoPessoa.codigo, tipoPessoa);
        }
    }

    public Integer codigo;

    private TipoPessoa(Integer codigo) {
        this.codigo = codigo;
    }

    public static TipoPessoa fromCodigo(Integer codigo) {
        if (lookup.containsKey(codigo)) {
            return lookup.get(codigo);
        }
        throw new IllegalArgumentException(String.format("Código do TipoPessoa inválida: %d", codigo));
    }

    public Integer getCodigo() {
        return codigo;
    }
}
