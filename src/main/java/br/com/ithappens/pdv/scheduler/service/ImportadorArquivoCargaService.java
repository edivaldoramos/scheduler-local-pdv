package br.com.ithappens.pdv.scheduler.service;

import br.com.ithappens.pdv.scheduler.mapper.CargaMapper;
import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.model.dto.wsmateus.FileData;
import br.com.ithappens.pdv.scheduler.model.enums.StatusCarga;
import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import br.com.ithappens.pdv.scheduler.service.exception.CargaComImportacaoInterrompidaException;
import br.com.ithappens.pdv.scheduler.service.exception.CargaNuncaImportadaException;
import br.com.ithappens.pdv.scheduler.service.validation.IValicaoArquivoCargaService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;

@Slf4j
public abstract class ImportadorArquivoCargaService {
    @Autowired
    CargaMapper cargaMapper;

    @Autowired
    IValicaoArquivoCargaService valicaoArquivoCargaService;

    /**
     * Método deve importar uma carga do tipo passado considerando a última carga gerada na tabela de controle local. Caso nenhuma importação
     * tenha sido feita uma exceção do tipo CargaNuncaImportadaException é lançada para que então alguém a trate fazendo uma primeira importação.
     * <p>
     * Quando não se lança a exceção este método importa uma carga com base em um controle de cargas existente, este processo está dividido
     * nas seguintes etapas:
     * <p>
     * 1. Recuperação da última carga para o tipo de carga passada pois caso exista sua data será utilizada como referência para recuperar
     * um hash de carga mais recente ou não, caso não exista nenhuma carga significa que está é a primeira importação e uma exceção é lançada.
     * 2. Se tenta recuperar um hash de carga disponível para download com data maior ou igual a da última carga.
     * 3. Download da carga com base no hash recuperado.
     * 4. Descompactação do arquivo (que vem em formato 7z).
     * 5. Atualização do banco de dados (inserindo registros novos e atualizando existentes) conforme a planilha CSV que foi descompactada.
     * 6. Controle local de importação de cargas é dado como finalizado.
     * 7. Controle externo de cargas disponíveis é notificado de que carga foi importada com sucesso.
     *
     * @param tipoCarga
     * @throws CargaNuncaImportadaException
     * @throws IOException
     * @author Allyx Cristiano
     */
    public void importaCargaParcial(TipoCarga tipoCarga) throws CargaNuncaImportadaException, IOException, CargaComImportacaoInterrompidaException, InterruptedException {
        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Inicio da recuperação de última carga, tipo de carga: " + tipoCarga);
        }

        Carga ultimaCarga = cargaMapper.recuperarUltimaCarga(tipoCarga);

        valicaoArquivoCargaService.validarCargaReferenciaParaImportacao(ultimaCarga);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da recuperação de última carga não processada e inicio do download, tipo de carga: " + tipoCarga);
        }

        Carga carga = new Carga(TipoCarga.CLIENTE);

        String hash = this.getHashCargaPorData(ultimaCarga.getDataCriacao());

        valicaoArquivoCargaService.validarHash(hash);

        carga.setHashArquivo(hash);

        this.notificarInicioProcessamentoCarga(hash);

        FileData arquivo = this.efetuaDownload(hash);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim do download e inicio da descompactação, tipo de carga: " + tipoCarga);
        }

        this.descompacta(arquivo);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da descompactação e inicio da atualização do CSV com o banco de dados, tipo de carga: " + tipoCarga);
        }

        carga.setStatus(StatusCarga.PROCESSANDO);
        cargaMapper.inserir(carga);

        this.atualizaBancoDadosLocalComDadosArquivoCSV();

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da atualização do CSV com o banco de dados, tipo de carga: " + tipoCarga);
        }

        carga.setStatus(StatusCarga.PROCESSADO);
        cargaMapper.atualizar(carga);

        this.notificarFimProcessamentoCarga(hash);
    }

    public void importaCargaCompleta() throws IOException, InterruptedException {
        Carga carga = new Carga(TipoCarga.CLIENTE);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Inicio do download, tipo de carga: " + this.getClass().getName());
        }

        String hash = this.getHashCarga();

        valicaoArquivoCargaService.validarHash(hash);

        carga.setHashArquivo(hash);

        this.notificarInicioProcessamentoCarga(hash);

        FileData arquivo = this.efetuaDownload(hash);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim do download e inicio da descompactação, tipo de carga: " + this.getClass().getName());
        }

        this.descompacta(arquivo);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da descompactação e inicio da atualização do CSV com o banco de dados, tipo de carga: " + this.getClass().getName());
        }

        carga.setStatus(StatusCarga.PROCESSANDO);
        cargaMapper.inserir(carga);

        this.atualizaBancoDadosLocalComDadosArquivoCSV();

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da atualização do CSV com o banco de dados, tipo de carga: " + this.getClass().getName());
        }

        carga.setStatus(StatusCarga.PROCESSADO);
        cargaMapper.atualizar(carga);

        this.notificarFimProcessamentoCarga(hash);
    }

    public void importarCargaInterrompida(Carga carga) throws IOException, InterruptedException {
        FileData arquivo = this.efetuaDownload(carga.getHashArquivo());

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim do download e inicio da descompactação, tipo de carga: " + this.getClass().getName());
        }

        this.descompacta(arquivo);

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da descompactação e inicio da atualização do CSV com o banco de dados, tipo de carga: " + this.getClass().getName());
        }

        this.atualizaBancoDadosLocalComDadosArquivoCSV();

        if (log.isDebugEnabled()) {
            Thread.sleep(100);
            log.debug("Fim da atualização do CSV com o banco de dados, tipo de carga: " + this.getClass().getName());
        }

        carga.setStatus(StatusCarga.PROCESSADO);
        cargaMapper.atualizar(carga);

        this.notificarFimProcessamentoCarga(carga.getHashArquivo());
    }

    abstract URL getUrlDownloadCompleto() throws MalformedURLException;

    abstract URL getUrlDownloadPorData(LocalDateTime dataHora) throws MalformedURLException;

    abstract String getHashCarga() throws IOException;

    abstract String getHashCargaPorData(LocalDateTime data) throws IOException;


    public String download(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setConnectTimeout(5000);
        httpURLConnection.setReadTimeout(5000);
        httpURLConnection.getResponseCode();

        valicaoArquivoCargaService.validarProcessamentoExterno(httpURLConnection);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
        String linhaResultado;
        StringBuffer conteudoRequisicao = new StringBuffer();

        while ((linhaResultado = bufferedReader.readLine()) != null) {
            conteudoRequisicao.append(linhaResultado);
        }

        bufferedReader.close();
        httpURLConnection.disconnect();

        return conteudoRequisicao.toString();
    }

    abstract FileData efetuaDownload(String hash) throws IOException;

    abstract void descompacta(FileData arquivo) throws IOException;

    abstract void atualizaBancoDadosLocalComDadosArquivoCSV() throws IOException;

    public String getCaminhoPastaUsuario() {
        return System.getProperty("user.home");
    }

    abstract String getNomeArquivoCSV();

    public String getCaminhoArquivoCSV() {
        return this.getCaminhoPastaUsuario() + "/" + this.getNomeArquivoCSV();
    }

    public CSVReader getCsvReader() throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(this.getCaminhoArquivoCSV()));

        CSVParser parser = new CSVParserBuilder()
                .withSeparator('|')
                .withQuoteChar('"')
                .withEscapeChar('`')
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        return csvReader;
    }

    String[] getLinhaComCamposComConteudoAjustado(String[] linha) {
        for (int i = 0; i < linha.length; i++) {
            linha[i] = linha[i].trim();
        }

        return linha;
    }

    void notificarInicioProcessamentoCarga(String hash) throws IOException {
        URL url = new URL("http://127.0.0.1:8086/clientes/hash/processa/" + hash);
        this.download(url);
    }

    void notificarFimProcessamentoCarga(String hash) throws IOException {
        URL url = new URL("http://127.0.0.1:8086/clientes/hash/finaliza/" + hash);
        this.download(url);
    }
}