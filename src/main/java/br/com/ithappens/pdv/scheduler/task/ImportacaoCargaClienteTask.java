package br.com.ithappens.pdv.scheduler.task;

import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import br.com.ithappens.pdv.scheduler.service.ImportadorArquivoCargaService;
import br.com.ithappens.pdv.scheduler.service.exception.CargaComImportacaoInterrompidaException;
import br.com.ithappens.pdv.scheduler.service.exception.CargaNuncaImportadaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class ImportacaoCargaClienteTask implements IImportacaoCargaClienteTask {
    private final long SEGUNDO = 1000;
    private final long MINUTO = SEGUNDO * 60;
    private final long HORA = MINUTO * 60;

    @Autowired
    private ImportadorArquivoCargaService importadorArquivoCargaService;

    @Scheduled(fixedDelay = MINUTO * 20)
    @Override
    public void importarCarga() throws IOException, InterruptedException {
        if (log.isInfoEnabled()) log.info("Inicio da importacao parcial da carga de cliente");
        try {
            importadorArquivoCargaService.importaCargaParcial(TipoCarga.CLIENTE);
            if (log.isInfoEnabled()) log.info("Fim da importação parcial da carga de cliente");
        } catch (CargaNuncaImportadaException e) {
            if (log.isInfoEnabled()) log.info("Importação parcial sem data valida, inicio da importação completa da carga de cliente");
            importadorArquivoCargaService.importaCargaCompleta();
            if (log.isInfoEnabled()) log.info("Fim da importação completa da carga de cliente");
        } catch (CargaComImportacaoInterrompidaException e) {
            if (log.isInfoEnabled()) log.info("Retomando importação interrompida da carga de cliente");
            importadorArquivoCargaService.importarCargaInterrompida(e.getCarga());
            if (log.isInfoEnabled()) log.info("Fim da importação interrompida da carga de cliente");
        } catch (Throwable e) {
            if (log.isErrorEnabled()) log.error(e.getMessage(), e.getMessage());
        }
    }
}