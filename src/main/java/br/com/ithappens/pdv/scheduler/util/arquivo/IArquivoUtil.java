package br.com.ithappens.pdv.scheduler.util.arquivo;

import java.io.File;
import java.io.IOException;

public interface IArquivoUtil {

    /**
     * Método deverá fornecer uma compactação do arquivo para o destino passado.
     *
     * @param arquivo
     * @param destinoArquivoCompactado
     * @throws IOException
     * @author Edivaldo Ramos / Allyx Cristiano
     */
    void compactar(File arquivo, String destinoArquivoCompactado) throws IOException;

    void descompactar(File arquivo, String destinoArquivoDescompactado) throws IOException;
}
