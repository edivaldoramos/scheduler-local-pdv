package br.com.ithappens.pdv.scheduler.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class CargaClienteDTO {

    @Getter
    @Setter
    private Long id;

    @Setter
    private String razaoSocial;

    @Setter
    private String nomeFantasia;

    @Getter
    @Setter
    private String tipoPessoa;

    @Getter
    @Setter
    private String cpfOuCnpj;

    public String getRazaoSocial() {
        if (razaoSocial == null) {
            razaoSocial = new String("");
        }
        return razaoSocial.trim();
    }

    public String getNomeFantasia() {
        if (nomeFantasia == null) {
            nomeFantasia = new String("");
        }
        return nomeFantasia.trim();
    }
}
