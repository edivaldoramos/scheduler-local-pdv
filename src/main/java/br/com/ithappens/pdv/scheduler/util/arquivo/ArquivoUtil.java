package br.com.ithappens.pdv.scheduler.util.arquivo;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.sevenz.SevenZMethod;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ArquivoUtil implements IArquivoUtil {
    @Override
    public void compactar(File arquivo, String destinoArquivoCompactado) throws IOException {
        final File compactado = new File(destinoArquivoCompactado);
        final SevenZOutputFile sevenZOutputFile = new SevenZOutputFile(compactado);
        sevenZOutputFile.setContentCompression(SevenZMethod.BZIP2);
        FileInputStream fileInputStream = null;

        SevenZArchiveEntry entry = sevenZOutputFile.createArchiveEntry(arquivo, arquivo.getName());
        sevenZOutputFile.putArchiveEntry(entry);

        fileInputStream = new FileInputStream(arquivo);

        final byte[] buffer = new byte[8 * 1024];
        int bytesRead;
        while ((bytesRead = fileInputStream.read(buffer)) >= 0) {
            sevenZOutputFile.write(buffer, 0, bytesRead);
        }

        sevenZOutputFile.closeArchiveEntry();

        if (fileInputStream != null) fileInputStream.close();

        sevenZOutputFile.close();
    }

    @Override
    public void descompactar(File arquivo, String destinoArquivoDescompactado) throws IOException {
        SevenZFile sevenZFile = new SevenZFile(arquivo);
        SevenZArchiveEntry entry;
        while ((entry = sevenZFile.getNextEntry()) != null) {
            if (entry.isDirectory()) {
                continue;
            }
            File curfile = new File(destinoArquivoDescompactado, entry.getName());
            File parent = curfile.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            FileOutputStream out = new FileOutputStream(curfile);

            byte[] content = new byte[(int) entry.getSize()];
            sevenZFile.read(content, 0, content.length);
            out.write(content);
            out.close();
        }

        sevenZFile.close();
    }
}
