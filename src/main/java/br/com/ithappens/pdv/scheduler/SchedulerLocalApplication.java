package br.com.ithappens.pdv.scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchedulerLocalApplication {
    public static void main(String[] args) {
        SpringApplication.run(SchedulerLocalApplication.class, args);
    }
}

