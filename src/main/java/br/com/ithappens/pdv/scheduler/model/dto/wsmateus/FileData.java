package br.com.ithappens.pdv.scheduler.model.dto.wsmateus;

import java.io.Serializable;
import java.util.Date;

public class FileData implements Serializable {

    private boolean sucesso;
    private String categoria;
    private String nomeDoArquivo;
    private String observacoes;
    private String urlDownload;
    private Date armazenadoEm;
    private byte[] conteudo;

    public boolean isSucesso() {
        return sucesso;
    }

    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNomeDoArquivo() {
        return nomeDoArquivo;
    }

    public void setNomeDoArquivo(String nomeDoArquivo) {
        this.nomeDoArquivo = nomeDoArquivo;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getUrlDownload() {
        return urlDownload;
    }

    public void setUrlDownload(String urlDownload) {
        this.urlDownload = urlDownload;
    }

    public Date getArmazenadoEm() {
        return armazenadoEm;
    }

    public void setArmazenadoEm(Date armazenadoEm) {
        this.armazenadoEm = armazenadoEm;
    }

    public byte[] getConteudo() {
        return conteudo;
    }

    public void setConteudo(byte[] conteudo) {
        this.conteudo = conteudo;
    }

    @Override
    public String toString() {
        return "FileData{" +
                "sucesso=" + sucesso +
                ", categoria='" + categoria + '\'' +
                ", nomeDoArquivo='" + nomeDoArquivo + '\'' +
                ", observacoes='" + observacoes + '\'' +
                ", urlDownload='" + urlDownload + '\'' +
                ", armazenadoEm=" + armazenadoEm +
                '}';
    }
}
