package br.com.ithappens.pdv.scheduler.util;

import br.com.ithappens.pdv.scheduler.model.dto.wsmateus.FileData;

import java.io.IOException;

public interface IWSMateus {

    /**
     * Método faz o download baseaso em um HASH.
     *
     * @param hash
     * @return
     * @throws IOException
     */
    FileData download(String hash) throws IOException;

    /**
     * Método faz o download baseaso em um HASH e uma senha.
     *
     * @param hash
     * @param senha
     * @return
     * @throws IOException
     */
    FileData download(String hash, String senha) throws IOException;

    FileData metadados(String hash) throws IOException;

    FileData metadados(String hash, String senha) throws IOException;

    /**
     * Método faz upload do arquivo informando categoria, o arquivo e o seu nome.
     *
     * @param categoria
     * @param dados
     * @param nomeDoArquivo
     * @return
     * @throws IOException
     */
    String upload(String categoria, byte[] dados, String nomeDoArquivo) throws IOException;

    /**
     * Método faz upload do arquivo informando categoria, o arquivo, seu nome e uma observação.
     *
     * @param categoria
     * @param dados
     * @param nomeDoArquivo
     * @param observacoes
     * @return
     * @throws IOException
     */
    String upload(String categoria, byte[] dados, String nomeDoArquivo, String observacoes) throws IOException;

    /**
     * Método faz upload do arquivo informando categoria, o arquivo, seu nome, uma observação e uma senha.
     *
     * @param categoria
     * @param dados
     * @param nomeDoArquivo
     * @param observacoes
     * @param senha
     * @return
     * @throws IOException
     */
    String upload(String categoria, byte[] dados, String nomeDoArquivo, String observacoes, String senha) throws IOException;

}