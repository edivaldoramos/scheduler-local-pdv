package br.com.ithappens.pdv.scheduler.model;

import br.com.ithappens.pdv.scheduler.model.enums.StatusCarga;
import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
public class Carga {

    @Getter
    @Setter
    private Long id;

    @Getter
    private TipoCarga tipo;

    @Getter
    private LocalDateTime dataCriacao;

    @Getter
    private LocalDateTime dataInicioProcessamento;

    @Getter
    private LocalDateTime dataFimProcessamento;

    @Getter
    @Setter
    private String hashArquivo;

    @Getter
    private StatusCarga status;

    public Carga(TipoCarga tipo) {
        this.tipo = tipo;
        this.dataCriacao = LocalDateTime.now();
    }

    public void setStatus(StatusCarga status) {
        this.status = status;
        if (this.status == StatusCarga.PROCESSANDO) {
            this.dataInicioProcessamento = LocalDateTime.now();
        } else if (this.status == StatusCarga.PROCESSADO) {
            this.dataFimProcessamento = LocalDateTime.now();
        } else {
            throw new IllegalArgumentException("Status de carga inválido: " + status);
        }
    }

    public Boolean teveImportacaoInterrompida() {
        return this.naoEstaFinalizada() && this.contemHashValido();
    }

    private Boolean naoEstaFinalizada() {
        return this.getStatus() == StatusCarga.PROCESSANDO;
    }

    private Boolean contemHashValido() {
        return !this.hashArquivo.trim().isEmpty();
    }
}
