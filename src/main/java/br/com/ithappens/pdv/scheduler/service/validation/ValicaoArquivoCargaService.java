package br.com.ithappens.pdv.scheduler.service.validation;

import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.service.exception.CargaComImportacaoInterrompidaException;
import br.com.ithappens.pdv.scheduler.service.exception.CargaNuncaImportadaException;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.io.IOException;
import java.net.HttpURLConnection;

@Service
public class ValicaoArquivoCargaService implements IValicaoArquivoCargaService {
    @Override
    public void validarCargaReferenciaParaImportacao(Carga carga) throws CargaNuncaImportadaException, CargaComImportacaoInterrompidaException {
        if (carga == null) {
            throw new CargaNuncaImportadaException("Carga nunca foi importada");
        }

        if (carga.teveImportacaoInterrompida()) {
            throw new CargaComImportacaoInterrompidaException("Carga teve processo iniciado porém não finalizou", carga);
        }
    }

    @Override
    public void validarHash(String hash) {
        if (hash.trim().isEmpty()) {
            throw new ValidationException("Não existe nova carga disponível para download");
        }
    }

    @Override
    public void validarProcessamentoExterno(HttpURLConnection httpURLConnection) throws IOException {
        if (httpURLConnection == null) {
            throw new ValidationException("HttpURLConnection nulo");
        }

        if (httpURLConnection.getResponseCode() != 200) {
            throw new ValidationException(httpURLConnection.getHeaderField("Erro"));
        }
    }
}
