package br.com.ithappens.pdv.scheduler.model;

import br.com.ithappens.pdv.scheduler.model.enums.TipoPessoa;
import lombok.Data;

@Data
public class Cliente {
    Long id;
    String nomeFantasia;
    String razaoSocial;
    TipoPessoa tipoPessoa;
    Long cpfCnpj;
}
