package br.com.ithappens.pdv.scheduler.service.validation;

public interface IValicaoArquivoCargaClienteService {
    void validarLinhaTemQuantidadeEsperadaCampos(String[] linha);
}
