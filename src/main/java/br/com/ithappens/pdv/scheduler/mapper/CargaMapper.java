package br.com.ithappens.pdv.scheduler.mapper;

import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CargaMapper {
    Carga recuperarUltimaCarga(@Param("tipoCarga") TipoCarga tipoCarga);

    void inserir(@Param("carga") Carga carga);

    void atualizar(@Param("carga") Carga carga);
}
