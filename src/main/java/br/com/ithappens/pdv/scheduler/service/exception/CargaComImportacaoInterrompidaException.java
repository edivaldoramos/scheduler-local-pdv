package br.com.ithappens.pdv.scheduler.service.exception;

import br.com.ithappens.pdv.scheduler.model.Carga;
import lombok.Getter;

public class CargaComImportacaoInterrompidaException extends Exception {

    @Getter
    Carga carga;

    public CargaComImportacaoInterrompidaException(String errorMessage, Carga carga) {
        super(errorMessage);
        this.carga = carga;
    }
}
