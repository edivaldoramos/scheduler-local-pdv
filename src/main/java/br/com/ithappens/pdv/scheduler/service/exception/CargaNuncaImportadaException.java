package br.com.ithappens.pdv.scheduler.service.exception;

public class CargaNuncaImportadaException extends Exception {
    public CargaNuncaImportadaException(String errorMessage) {
        super(errorMessage);
    }
}
