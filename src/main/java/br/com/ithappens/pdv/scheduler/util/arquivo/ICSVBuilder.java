package br.com.ithappens.pdv.scheduler.util.arquivo;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.IOException;
import java.util.List;

public interface ICSVBuilder<T> {

    /**
     * Os nomes das colunas do arquivo CSV (primeira linha do arquivo) deverão ser adicionados por este método.
     *
     * @param headers
     * @return
     * @author Allyx Cristiano
     */
    ICSVBuilder adicionarLinhaHeader(String[] headers);

    /**
     * Os nomes dos atributos da classe <T> que será passada deverão ser passados na ordem das colunas do header por este método.
     *
     * @param campos
     * @return
     * @author Allyx Cristiano
     */
    ICSVBuilder adicionarCamposLeitura(String[] campos);

    /**
     * Onde o arquivo CSV deverá ser salvo deverá ser informado por este método.
     *
     * @param caminho
     * @return
     * @author Allyx Cristiano
     */
    ICSVBuilder adicionarCaminhoSalvarArquivo(String caminho);

    /**
     * Método responsável por consumir a lista genérica, headers e campos do dominio generico para então construir a planilha CSV.
     *
     * @param lista
     * @return
     * @author Edivaldo Ramos / Allyx Cristiano
     */
    ICSVBuilder construir(List<T> lista) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException;

    /**
     * Método deverá ser responsável por construir de forma incremental, adicionando valor a valor a um builder existente.
     *
     * @param valor
     * @return
     * @author Edivaldo Ramos / Allyx Cristiano
     */
    ICSVBuilder construir(T valor) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException;

    /**
     * Método responsável por fechar o arquivo que está sendo escrito.
     *
     * @throws IOException
     * @author Edivaldo Ramos / Allyx Cristiano
     */
    void closeFileWriter() throws IOException;
}
