package br.com.ithappens.pdv.scheduler.service.validation;

import org.springframework.stereotype.Service;

import javax.validation.ValidationException;

@Service
public class ValicaoArquivoCargaClienteService implements IValicaoArquivoCargaClienteService {
    @Override
    public void validarLinhaTemQuantidadeEsperadaCampos(String[] linha) {
        if (linha.length != 5)
            throw new ValidationException("Linha não contém quantidade de campos esperada para se montar o objeto do tipo Cliente, linha:" + linha);
    }
}
