package br.com.ithappens.pdv.scheduler.mapper.typeHandler;

import br.com.ithappens.pdv.scheduler.model.enums.StatusCarga;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatusCargaTypeHandler extends BaseTypeHandler<StatusCarga> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, StatusCarga parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getCodigo());
    }

    @Override
    public StatusCarga getNullableResult(ResultSet rs, String columnName) throws SQLException {
        StatusCarga statusCarga = StatusCarga.fromCodigo(rs.getInt(columnName));
        if (rs.wasNull())
            return null;
        return statusCarga;
    }

    @Override
    public StatusCarga getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        StatusCarga statusCarga = StatusCarga.fromCodigo(rs.getInt(columnIndex));
        if (rs.wasNull())
            return null;
        return statusCarga;
    }

    @Override
    public StatusCarga getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        StatusCarga statusCarga = StatusCarga.fromCodigo(cs.getInt(columnIndex));
        if (cs.wasNull())
            return null;
        return statusCarga;
    }
}
