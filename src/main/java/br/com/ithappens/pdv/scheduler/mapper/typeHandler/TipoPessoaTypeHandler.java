package br.com.ithappens.pdv.scheduler.mapper.typeHandler;

import br.com.ithappens.pdv.scheduler.model.enums.TipoPessoa;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TipoPessoaTypeHandler extends BaseTypeHandler<TipoPessoa> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, TipoPessoa parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getCodigo());
    }

    @Override
    public TipoPessoa getNullableResult(ResultSet rs, String columnName) throws SQLException {
        TipoPessoa tipoPessoa = TipoPessoa.fromCodigo(rs.getInt(columnName));
        if (rs.wasNull())
            return null;
        return tipoPessoa;
    }

    @Override
    public TipoPessoa getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        TipoPessoa tipoPessoa = TipoPessoa.fromCodigo(rs.getInt(columnIndex));
        if (rs.wasNull())
            return null;
        return tipoPessoa;
    }

    @Override
    public TipoPessoa getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        TipoPessoa tipoPessoa = TipoPessoa.fromCodigo(cs.getInt(columnIndex));
        if (cs.wasNull())
            return null;
        return tipoPessoa;
    }
}