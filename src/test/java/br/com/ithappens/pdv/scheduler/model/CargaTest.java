package br.com.ithappens.pdv.scheduler.model;

import br.com.ithappens.pdv.scheduler.model.enums.StatusCarga;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CargaTest {

//    public Carga(TipoCarga tipo) {
//        this.tipo = tipo;
//        this.dataCriacao = LocalDateTime.now();
//    }
//
//    public void setStatus(StatusCarga status) {
//        this.status = status;
//        if (this.status == StatusCarga.PROCESSANDO) {
//            this.dataInicioProcessamento = LocalDateTime.now();
//        } else if (this.status == StatusCarga.PROCESSADO) {
//            this.dataFimProcessamento = LocalDateTime.now();
//        } else {
//            throw new IllegalArgumentException("Status de carga inválido: " + status);
//        }
//    }

//
//    public Boolean teveImportacaoInterrompida() {
//        return this.naoEstaFinalizada() && this.contemHashValido();
//    }
//
//    private Boolean naoEstaFinalizada() {
//        return this.getStatus() == StatusCarga.PROCESSANDO;
//    }
//
//    private Boolean contemHashValido() {
//        return !this.hashArquivo.trim().isEmpty();
//    }

    @Test
    public void aoModificarStatusParaProcessandoDataInicioProcessamentoDeveSerPreenchida() {

        Carga carga = new Carga();
        carga.setStatus(StatusCarga.PROCESSANDO);
        Assert.assertTrue(carga.getDataInicioProcessamento() != null);

    }

    @Test
    public void aoModificarStatusParaProcessadoDataInicioProcessamentoDeveSerPreenchida() {
        Carga carga = new Carga();
        carga.setStatus(StatusCarga.PROCESSADO);
        Assert.assertTrue(carga.getDataFimProcessamento() != null);
    }

    @Test
    public void casoTeveImportacaoInterrompidaStatusDeveSerProcessandoEHashNaoDeveSerVazio(){
        Carga carga = new Carga();
        carga.setHashArquivo("asdjasdljk");
        carga.setStatus(StatusCarga.PROCESSANDO);
        Assert.assertTrue(carga.teveImportacaoInterrompida());
    }

}
