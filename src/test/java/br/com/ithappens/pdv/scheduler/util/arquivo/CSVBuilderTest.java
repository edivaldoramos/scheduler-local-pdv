package br.com.ithappens.pdv.scheduler.util.arquivo;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.Data;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CSVBuilderTest {

    List<CSVBuilderDomainTest> csvBuilderDomainTests;
    String id;
    String descricao;
    String idObjeto;
    String descricaoObjeto;

    CSVReader csvReader;
    List<String[]> linhasArquivo;

    public String getCaminhoArquivo() {
        return new String(System.getProperty("user.home")) + "/test.csv";
    }

    @Before
    public void executarAntesDeExecutarOsTestes() throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        this.id = "ID";
        this.descricao = "DESCRIÇÃO";

        this.idObjeto = String.valueOf(123L);
        this.descricaoObjeto = "Teste1";

        this.csvBuilderDomainTests = new ArrayList<>();

        CSVBuilderDomainTest csvBuilderDomainTest1 = new CSVBuilderDomainTest();
        csvBuilderDomainTest1.setId(Long.parseLong(this.idObjeto));
        csvBuilderDomainTest1.setDescricao(this.descricaoObjeto);

        this.csvBuilderDomainTests.add(csvBuilderDomainTest1);

        String[] headers = new String[]{this.id, this.descricao};
        String[] campos = new String[]{"id", "descricao"};

        ICSVBuilder<CSVBuilderDomainTest> csvBuilderDomainTestICSVBuilder = new CSVBuilder<>(CSVBuilderDomainTest.class);

        csvBuilderDomainTestICSVBuilder
                .adicionarCaminhoSalvarArquivo(this.getCaminhoArquivo())
                .adicionarLinhaHeader(headers)
                .adicionarCamposLeitura(campos)
                .construir(this.csvBuilderDomainTests);

        this.csvReader = new CSVReader(new FileReader(this.getCaminhoArquivo()));
        this.linhasArquivo = this.csvReader.readAll();
    }

    @Test
    public void arquivoDeveConterHeaderConformeEsperado() throws IOException {
        String linhaHeaderCompleto = this.linhasArquivo.get(0)[0];
        Assert.assertEquals(this.id, linhaHeaderCompleto.substring(0, 2));
        Assert.assertEquals(this.descricao, linhaHeaderCompleto.substring(5, 14));
    }

    @Test
    public void arquivoDeveConterRegistroConformeEsperado() {
        String linhaPrimeiroRegistro = this.linhasArquivo.get(1)[0];
        Assert.assertEquals(this.idObjeto, linhaPrimeiroRegistro.substring(0, 3));
        Assert.assertEquals(this.descricaoObjeto, linhaPrimeiroRegistro.substring(6, 12));
    }

    @Test
    public void arquivoDeveSerGeradoCaminhoEsperado() {
        File file = new File(this.getCaminhoArquivo());
        Assert.assertEquals(true, file.exists());
    }

    @Test
    public void arquivoDeveGerarQuantidadeLinhasConformeEsperado() {
        Assert.assertEquals(2, this.linhasArquivo.size());
    }

    @After
    public void executarAposExecutarOsTestes() throws IOException {
        Files.delete(Paths.get(this.getCaminhoArquivo()));
    }

    @Data
    public class CSVBuilderDomainTest {
        Long id;
        String descricao;
    }
}