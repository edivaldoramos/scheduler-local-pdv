package br.com.ithappens.pdv.scheduler.util.arquivo;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ArquivoUtilTest {

    File arquivo;
    String hashArquivoAntesCompactacao;

    public static String getCaminhoUsuario() {
        return new String(System.getProperty("user.home"));
    }

    public static String getNomeArquivoDescompactado() {
        return "/test2.csv";
    }

    public static String getCaminhoArquivo() {
        return getCaminhoUsuario() + "/test.csv";
    }

    public static String getCaminhoArquivoDescompactado() {
        return getCaminhoUsuario() + getNomeArquivoDescompactado();
    }

    public static String getCaminhoArquivoCompactado() {
        return getCaminhoUsuario() + "/test.7z";
    }

    @Before
    public void executarAntesDeExecutarOsTestes() throws IOException {
        this.arquivo = new File(this.getCaminhoArquivo());
        this.arquivo.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(this.arquivo));
        writer.write("hello");
        writer.close();

        IArquivoUtil iArquivoUtil = new ArquivoUtil();
        iArquivoUtil.compactar(this.arquivo, this.getCaminhoArquivoCompactado());
        FileInputStream fileInputStream = new FileInputStream(this.arquivo);

        this.hashArquivoAntesCompactacao = DigestUtils.md5Hex(fileInputStream);
    }

    @Test
    public void hashArquivoDescompactadoDeveSerIgualHashArquivoOriginal() throws IOException {
        SevenZFile sevenZFile = new SevenZFile(new File(this.getCaminhoArquivoCompactado()));
        SevenZArchiveEntry sevenZArchiveEntry = null;
        File file = null;

        while ((sevenZArchiveEntry = sevenZFile.getNextEntry()) != null) {
            if (sevenZArchiveEntry.isDirectory()) {
                continue;
            }

            file = new File(this.getCaminhoUsuario(), this.getNomeArquivoDescompactado());

            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] content = new byte[(int) sevenZArchiveEntry.getSize()];
            sevenZFile.read(content, 0, content.length);
            fileOutputStream.write(content);
            fileOutputStream.close();
        }

        FileInputStream fileInputStream = new FileInputStream(file);
        String hashArquivoAposDescompactacao = DigestUtils.md5Hex(fileInputStream);
        Assert.assertEquals(this.hashArquivoAntesCompactacao, hashArquivoAposDescompactacao);
    }

    @After
    public void executarAposExecutarOsTestes() throws IOException {
        Files.delete(Paths.get(this.getCaminhoArquivo()));
        Files.delete(Paths.get(this.getCaminhoArquivoCompactado()));
        Files.delete(Paths.get(this.getCaminhoArquivoDescompactado()));
    }
}
